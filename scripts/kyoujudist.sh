#!/bin/sh
TEMPDIR="archive"
ARCHIVEFILE="archive.zip"

echo "教授送付用データを作成します"

if [ -d $TEMPDIR ] ; then
    echo 'Removing working directory...'
    rm -r $TEMPDIR
    echo 'Done.'
fi

if [ -f $ARCHIVEFILE ] ; then
    echo 'Removing archive file...'
    rm $ARCHIVEFILE
    echo 'Done.'
fi

mkdir $TEMPDIR
cp *.tex $TEMPDIR/
cp -r fig $TEMPDIR/
cp *.sty $TEMPDIR/

echo "Changing directory into [ $TEMPDIR ] ..."
cd $TEMPDIR

# remove emacs annotation
echo 'Removing emacs annotations (-*-)...'
#find . -iname '*.tex' -type f | xargs -t -I{}
sed -i '' -e'/-\*-.*-\*-$/d' *.tex # {}
echo 'Done.'

# convert encoding
echo 'Converting encoding from into cp932 with CRLF...'
nkf -sLw --overwrite *.tex
echo 'Done.'

cd ../
# Make archive file
echo 'Making zip file...'
zip -r $ARCHIVEFILE $TEMPDIR
echo 'Done.'

# Remove working directory
    echo 'Removing working directory...'
    rm -r $TEMPDIR
    echo 'Done.'

# Done!
echo "Archive file [ $ARCHIVEFILE ] has been created."
