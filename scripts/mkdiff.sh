#!/bin/sh
# mkdiff.sh
ROOTFILE=$1
TEMPDIR="/tmp/"
GITDIR=`pwd`

# work in HEAD
git stash save
OLDNAME=`git rev-parse HEAD`
NEWNAME="${OLDNAME}-working"
perl ./scripts/latexpand.pl ${ROOTFILE} | iconv -t UTF-8-MAC | iconv -t UTF-8 > ${OLDNAME}.tex

# work in working dir.
git stash pop
perl ./scripts/latexpand.pl ${ROOTFILE} | iconv -t UTF-8-MAC | iconv -t UTF-8 > ${NEWNAME}.tex

# jlatexdiff: latexdiffを日本語LaTeX文書でうまく扱えるようにする
# 参考：http://blogs.yahoo.co.jp/pentainformation/28571072.html

echo "HEADとWorking Treeとの差分を書き出します．"

# latexdiffで差分作成．
latexdiff -e utf8 ${OLDNAME}.tex ${NEWNAME}.tex > ${ROOTFILE}-diff.tex

# 改行がうまくできるようにsedで書き換え
sed -e 's/}\\DIFdelend/ }\\DIFdelend/g' -e 's/}\\DIFaddend/ }\\DIFaddend/g' ${ROOTFILE}-diff.tex > tmp.txt
mv tmp.txt ${ROOTFILE}-diff.tex
rm -f tmp.txt


# platex実行
# latexmk を利用する場合
#latexmk -pdfdvi ${fname}-diff.tex 

# platexを利用する場合 念のため3回実行
# platex ${fname}-diff.tex 
# platex ${fname}-diff.tex 
# platex ${fname}-diff.tex 
# dvipdfmx ${fname}-diff.dvi

simpdftex platex --mode dvipdfmx --extratexopts "-file-line-error -interaction=batchmode -kanji=utf8" ${ROOTFILE}-diff.tex

# 掃除
rm -f ${OLDNAME}.tex ${NEWNAME}.tex

exit 0
